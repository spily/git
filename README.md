# 处cp app-登录-支付-js代码调试

## 简介
处CP具有丰富的用户资源和渠道资源，这些资源可以为第三方企业快速引流、快速促进业务发展、快速实现经济效益等方面提供帮助。为了让第三方客户端可以简单快捷的接入到全民吃瓜的生态，全民吃瓜在技术层面提供了开放能力支撑。这其中包括，移动端APP开放能力支撑、小程序端APP开放能力支撑和H5页面开放能力支撑。

- 第三方移动端APP

第三方移动端APP是指使用IOS 或 Android开发的应用程序，它可以使用处CP进行登录，同时可以通过access token访问处CP的其他资源

- 第三方小程序

第三方小程序是指标准微信小程序，标准微信小程序可以运行在处CP中，处CP提供了兼容微信小程序的能力，开发者只用修改少量源码，甚至不用修改源码而将微信小程序运行在处CP中。第三方微信小程序有能力使用处CP提供的资源，如获取用户信息，获取用户地理位置等。

- 第三方 H5页面

第三方H5页面，是指使用H5 + Javascript开发的页面，它同样可以运行在小程序中，使用小程序的资源。


## 业务架构

![image](loginAndPayment.jpg)


## 业务详解

### 登录流程

1. H5客户端通过调用 `$cgapp.login()` 发起处CP登录, 该方法会吊起用户授权界面，请求用户授权; 如果用户已经授权登录到H5页面，则不会再次吊起该页面，除非H5客户端申请访问的信息，不在用户已授权列表中。

2. 当处CP获取到用户授权之后，它会将经过用户许可的信息返回给H5客户端。

3. H5客户端接收到用户信息之后，可以选择将用户信息回传到他们自己的服务器，从而创建session，以便后续使用；用户信息的使用方式，由H5客户端自己定义和实现。


### 支付流程

1. H5客户端发起支付请求之前，需要预先保存交易信息到其持久仓库

2. H5后端服务器响应一个已持久化的交易信息，包含订单号等

3. H5客户端通过调用`$cgapp.requestPayment()`方法，并传入订单信息从而发起支付请求；

4. 处CP接收到支付请求后，会吊起聚合支付页面，请求用户支付；同时，将用户的支付信息保存到处CP后端服务器

5. 处CP后端服务器会通知处CP客户端支付信息保存成功，并返回支付ID到处CP客户端；同时，会通知H5客户端的后端服务器支付结果

6. 处CP得到支付结果之后，会通知H5客户端支付结果；

7. H5 后端服务器，可以随时向处CP支付结果过查询API，查询支付结果


## API

调用`cgapp-js-sdk` 任何方法时，如果发生失败， H5客户端都会接收到一个 `CGAPPError`， 第三方H5客户端的开发者，可以通过验证错误码的方式，来判断错误类型，从而进行相应的处理。具体验证方式，请参考：
````
$cgapp.login({ scopes: ['openid']}, function(err, result){
if(err.code === 'ILLEGAL_PARAMETERS') {
// Illegal parameters error
}
})
````

错误码，请参考各个方法的API



### 登录 API

_格式_

`$cgapp.login(options, callback)`

_参数_

`options`, _JSON_ **必须**

- `options.scopes`, _Array{ String }_ **必须** 必须包含openid声明，例如： `scopes: ['openid']`

`callback(err, result)`, _Function_ **可选**

- `err` _CGAPPError_ 登录过程中，如果发生失败，则会返回err; 如果成功，则 err的值为null

- `result` _JSON_ 代表用户信息的JSON对象；如果登录失败，则result值为空

> `result` 里面的数据是基于请求的`scope`来定义的，例如，当`scopes:['openid', 'username']`, 则result的数据是 `{sid: 'xxx'， username: 'xxx'}`，当 `scopes: ['openid', 'username', 'phone_number']` 则result的数据是: `{sid: 'xxx', 'username': 'xxxx', phone_number: '1111111'}`。具体scope的定义，请参考文档最后的附录。

_错误码_

- `USER_CANCEL_LOGIN`, User has cancelled login

- `USER_DENIES_LOGIN`, User has denied login

- `UNKNOWN`, All other unknow errors


_示例_

````
$cgapp.login({ scopes: ['openid', 'phone_number']}, function(err, result){
if(err) {
// handle error here
} else {
// handle userinfo here
}
})
````

### 支付 API

_格式_

`$cgapp.requestPayment(options, callback)`

_参数_

`options`, _JSON_ **Required**

- `options.goodsName`, _String_ **Required** 商品信息

- `options.goodsDesc`, _String_ **Required** 商品详细描述信息

- `options.notifyUrl`, _String_ **Required** 接受支付结果的RESTful API， 由申请接入方提供；

- `options.orderNo`, _String_ **Required** 订单编号，由申请接入方提供

- `options.payType`, _Number_ **Required** `1` 支付宝支付, `2` 微信支付, `7` 钱包支付， 推荐优先使用 钱包支付 

- `options.money`, _String_ **Required** 支付金额; 可以精确到小数点后 `2` 位

- `opention.appId`, _String_ **Required** app id

`callback(err, result)`, _Function_ **Optional**

- `err` _CGAPPError_ 如果支付成功，则err的值为null; 如果支付失败err的值为`CGAPPError` 对象

- `result` _JSON_ 如果支付成功，result的值为 支付信息； 如果支付失败，result的值为null；  支付成功后，result的结构是 `{ payOrderNo: 'IWD15510990000001242889956407'}`, 既 result 仅包含 `payOrderNo` 字段。


_错误码_

- `USER_CANCEL_PAYMENT`, User has cancelled payment

- `USER_DENIES_PAYMENT`, User has denied PAYMENT

- `UNKNOWN`, All other unknow errors

_示例_

````
$cgapp.requestPayment({ goodsName: 'test goods', goodsDesc: 'test goods', notifyUrl: 'http://YOUR_URL', orderNo: 'abcxyz', payType: 2, money: 100.01 }, function(err, result){
if(err) {
// handle error here
} else {
// handle payment result here
}
})
````

### 分享 API

_格式_

`$cgapp.shareGame(options)`

_参数_

`options`, _JSON_ **Required**

- `options.title`, _String_ **Required** 分享内容的标题

- `options.desc`, _String_ **Required** 分享内容的描述

- `options.thumbData`, _String_ **Required** 分享内容的图片URL

- `options.name`, _String_ **Required** 分享内容的名字

- `options.icon`, _String_ **Required** 分享内容的图标URL 

- `options.tryLoadUrl`, _String_ **Required** 点击分享跳转的URL

_示例_

````
$cgapp.shareGame({
title:"share title",
desc:"share description",
thumbData:"share image url",
name:"share name",
icon:"share icon url",
tryLoadUrl:"click translate url"
})
````

## 集成指导

1. 第三方企业，首先需要向处CP提交集成申请；处CP会审核企业的申请，并基于审核规则，选择批准或拒绝申请

2. 第三方企业的申请获批后，需要向处CP提供一个接受支付结果的API，同时要确保该API具有足够的安全性

3. 第三方企业将`cgapp-js-sdk` 到其H5页面，具体可参考

## 集成指导
1. 而大方
2. 
````
<head>
<script src="http://alfs.idianyou.cn/dianyou/js/cgapp-js-sdk-1.0.0.min.js"></script>
</head>
````

4. 第三方企业在其H5客户端上，使用`cgapp-js-sdk`集成处CP的能力

5. 第三方企业提交其代码到处CP瓜审核平台，处CP瓜审核代码并且确认代码可以部署后，将会部署到处CP APP

## 本地调试

`cpagg-js-sdk` 支持本地调试代码，本地调试通过后，再部署到处CP生产环境之中。

本地调试步骤：

1. 部署集成了`cgapp-js-sdk`的应用到内网服务器，并确保已部署的应用可以在内网访问；

> 例如，假如内网服务器地址为 `192.168.1.99` 应用监听端口 `8080`, 则部署完成之后，需要通过多台内网机器验证 `http://192.168.1.99:8080` 是否可以成功访问！


2. 点击下载[处CP内测版], 并安装到 android 手机中


3. 确保安装了`处CP`的 android 手机，已经连接到您的内网；并且可以通过手机浏览器访问`步骤 1`部署的内网应用

4. 在处CP内调试代码

- 打开处CP，登录，然后点击底部`消息`，进入`消息`模块

- 在消息模块中，点击任意好友，打开聊天界面

- 在聊天界面中，输入`步骤 1`部署的应用地址。例如 `http://192.168.1.99:8080`， 然后点击发送

- 发送成功后，点击聊天窗口发送的链接(例如上述的`http://192.168.1.99:8080`)，打开您的应用（将会在处CP的新窗口中打开）

- 在新窗口，点击右上角`...`图标，可以打开或关闭`控制台`，从而在控制台查看您应用内的 `log`

> 注意事项：如果在处CP内无法访问您的内网应用，请检查处CP是否已经连接到内网（WIFI）； 同时确保 IP 地址设置正确；最后检查 服务器端口是否开放等...

iOS调试步骤：

1.设置`safari`浏览器

- 安装`safari`浏览器，打开safari的设置, 选择`偏好设置`，点击进入

- 选择设置的`高级`tab

- 保证在`在菜单栏中显示“开发”菜单”`为勾选状态
<img width="458" height="244" src="safari_setting_flow.jpg"/>


2.iPhone手机设置

- 打开iphone`设置`页面,找到`Safari浏览器`，点击进入

- 找到safari设置中的`高级`选项,点击进入

- 保证高级选项中的`Web检查器`为勾选状态
<img width="808" height="423" src="iPhone_setting_flow.jpg"/>


3.打开Safari的Web Inspector工具调试

- 用iPhone连接电脑并打开`处CP`App

- 选择处CP App的`消息`菜单 ->点击`联系人`tab ->选择任意`联系人` ->点击进入`聊天界面` ->在输入框内输入`需要调试的H5链接` ->点击发送
> 详细处CP内代码调试，参照`本地调试` `步骤4`

- 点击发送的H5链接，跳转到`H5界面`,然后打开电脑的`Safari`软件，选择`开发` ->`iPhone设备名字` ->`对应的H页面`点击进入 ->启动`Web Inspector`
<img width="875" height="310" src="ios_debugging_flow.jpg"/>


## 附录

### Scope

- openid 获取用户在处CP中的唯一标识

- username 获取用户名

- phone_number 获取用户的手机号

- location 获取用户的GIS位置

- address 获取用户的邮寄地址

>  其他scope会随着处CP的发展，不断的增加。


